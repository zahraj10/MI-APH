export const ATTR_AI_MODEL = "AI_MODEL";
export const ATTR_FACTORY = "FACTORY";
export const ATTR_AGENT_MODEL = "AGENT_MODEL";

export const AGENT_STATE_IDLE = 0;
export const AGENT_STATE_GOING_TO_LOAD = 1;
export const AGENT_STATE_GOING_TO_UNLOAD = 2;
export const AGENT_STATE_LOADING = 3;
export const AGENT_STATE_UNLOADING = 4;

export const AGENT_TYPE_RED = 1;
export const AGENT_TYPE_BLUE = 2;

export const CARGO_TYPE_ORE = 1;
export const CARGO_TYPE_PETROL = 2;

export const TEXTURE_AI = "TEXTURE_AI";

export const MAP_BLOCK_PATH = 0;
export const MAP_BLOCK_WALL = 1;
export const MAP_BLOCK_WAREHOUSE = 2;
export const MAP_BLOCK_ORE = 3;
export const MAP_BLOCK_PETROL = 4;

export const MAP_BLOCK_SIZE = 128;

