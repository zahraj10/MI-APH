import { FLAG_ENEMY_PROJECTILE } from './Constants';
import { checkTime } from './Utils';
import { SpacewarsBaseCmp } from './SpacewarsBaseCmp';

/**
 * Enemy shot logic
 */
export class EnemyComponent extends SpacewarsBaseCmp {
    lastShotTime = 0;
    shotFrequency = 0;

    onInit() {
        super.onInit();
        this.shotFrequency = this.model.shotFrequency;
    }

    onUpdate(delta: number, absolute: number) {
        if (checkTime(this.lastShotTime, absolute, 10)) {
            this.lastShotTime = absolute;

            // shoot probability
            if (!this.model.isPaused && Math.random() < this.model.shotFrequency) {
                this.factory.createProjectile(this.owner, this.model, FLAG_ENEMY_PROJECTILE);
            }
        }
    }
}