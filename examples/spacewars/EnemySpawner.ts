import { MSG_ENEMY_KILLED, TAG_ENEMY, MSG_ENEMY_CREATED } from './Constants';
import Msg from '../../ts/engine/Msg';
import { checkTime } from './Utils';
import { SpacewarsBaseCmp } from './SpacewarsBaseCmp';

/**
 * Global component responsible for creating new enemies
 */
export class EnemySpawner extends SpacewarsBaseCmp {
    lastSpawnTime = 0;
    spawnFrequency = 0;

    onInit() {
        super.onInit();
        this.subscribe(MSG_ENEMY_KILLED);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_ENEMY_KILLED && msg.gameObject.proxy.tag == TAG_ENEMY) {
            // sync number of enemies on the scene
            this.model.enemiesCreated--;
        }
    }

    onUpdate(delta, absolute) {
        if (checkTime(this.lastSpawnTime, absolute, 10)) {
            this.lastSpawnTime = absolute;

            // create a new enemy and notice other components
            if (!this.model.isPaused && Math.random() < this.model.enemySpawnFrequency && this.model.enemiesCreated < this.model.enemyMaxCount) {
                this.model.enemiesCreated++;
                this.factory.createEnemy(this.owner, this.model);
                this.sendMessage(MSG_ENEMY_CREATED);
            }
        }
    }
}