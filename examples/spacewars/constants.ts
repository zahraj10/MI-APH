// message keys
export const MSG_ENEMY_CREATED = "enemy_created";
export const MSG_ENEMY_KILLED = "unit_killed";
export const MSG_PROJECTILE_SHOT = "projectile_shot";
export const MSG_LEVEL_COMPLETED = "level_completed";
export const MSG_PAUSE = "pause";
export const MSG_GAME_OVER = "game_over";
export const MSG_ANIM_ENDED = "anim_ended";
export const MSG_COLLISION = "collision";

// attribute keys
export const ATTR_MODEL = "model";
export const ATTR_FACTORY = "factory";

// flags
export const FLAG_PLAYER_1_PROJECTILE = 1;
export const FLAG_PLAYER_2_PROJECTILE = 2;
export const FLAG_ENEMY_PROJECTILE = 3;
export const FLAG_COLLIDABLE = 4;

// enemy states
export const STATE_ALIVE = 1;
export const STATE_DEAD = 2;

// tags for game objects
export const TAG_ENEMY = "enemy";
export const TAG_PLAYER_1 = "player_1";
export const TAG_PLAYER_2 = "player_2";
export const TAG_SCORE = "score";
export const TAG_GAMEOVER = "gameover";
export const TAG_LEVEL_COMPLETED = "level_completed";
export const TAG_PAUSED = "paused";
export const TAG_DAMAGE = "damage";
export const TAG_CONTROLS = "controls";
export const TAG_PROJECTILE = "projectile";


// alias for config file
export const DATA_JSON = "DATA_JSON";

// texture aliases
export const TEXTURE_PLAYER_1 = "player_1";
export const TEXTURE_PLAYER_2 = "player_2";
export const TEXTURE_ENEMY_1 = "enemy_1";
export const TEXTURE_ENEMY_2 = "enemy_2";
export const TEXTURE_ENEMY_3 = "enemy_3";
export const TEXTURE_ENEMY_4 = "enemy_4";
export const TEXTURE_ENEMY_5 = "enemy_5";
export const TEXTURE_ENEMY_6 = "enemy_6";
export const TEXTURE_PLAYER_1_PROJECTILE = "player_1_projectile";
export const TEXTURE_PLAYER_2_PROJECTILE = "player_2_projectile";
export const TEXTURE_ENEMY_PROJECTILE = "enemy_projectile";

// sound aliases
export const SOUND_FIRE = "fire";
export const SOUND_GAMEOVER = "gameover";
export const SOUND_KILL = "kill";

// height of the scene will be set to 50 units for the purpose of better calculations
export const SCENE_HEIGHT = 50;

// native height of the game canvas. If bigger, it will be resized accordingly
export const SPRITES_RESOLUTION_HEIGHT = 400;

// native speed of the game
export const GAME_SPEED = 1;