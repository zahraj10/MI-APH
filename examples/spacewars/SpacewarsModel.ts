export class SpacewarsModel {
    // ========================= dynamic data
    level;
    score;
    goal;
    enemySpawnFrequency;
    shotFrequency;
    damage;
    enemiesCreated;
    isGameOver;
    isPaused;
    // ========================= 


    // ========================= static data
    playerFireRate;
    firstLevelEnemySpawnFrequency;
    firstLevelShotFrequency;
    enemyReward;
    shootPenalty;
    projectileVelocity;
    firstLevelGoal;
    nextLevelGoalIncrease;

    // positions for enemy spawning (in relative units)
    enemySpawnMinY;
    enemySpawnMaxY;
    enemyMinVelocity;
    enemyMaxVelocity;
    enemyMaxCount;

    loadModel(data: any) {
        this.playerFireRate = data.fire_rate;
        this.firstLevelShotFrequency = data.first_level_shot_frequency;
        this.firstLevelEnemySpawnFrequency = data.enemy_spawn_frequency;
        this.enemyReward = data.enemy_reward;
        this.shootPenalty = data.shoot_penalty;
        this.projectileVelocity = data.projectile_velocity;
        this.enemySpawnMinY = data.enemy_spawn_min_y;
        this.enemySpawnMaxY = data.enemy_spawn_max_y;
        this.enemyMinVelocity = data.enemy_min_velocity;
        this.enemyMaxVelocity = data.enemy_max_velocity;
        this.enemyMaxCount = data.enemy_max_count;
        this.firstLevelGoal = data.first_level_goal;
        this.nextLevelGoalIncrease = data.next_level_goal_increase;
    }

    /**
     * Resets dynamic data
     */
    reset() {
        this.level = 1;
        this.score = 0;
        this.damage = 0;
        this.goal = this.firstLevelGoal;
        this.shotFrequency = this.firstLevelShotFrequency;
        this.enemySpawnFrequency = this.firstLevelEnemySpawnFrequency;
        this.enemiesCreated = 0;
        this.isGameOver = false;
        this.isPaused = false;
    }
}