import { DynamicsComponent } from '../../ts/components/DynamicsComponent';
import { SpacewarsModel } from './SpacewarsModel';
import { ATTR_MODEL } from './Constants';

/**
 * Enemy movement logic
 */
export class EnemyMovement extends DynamicsComponent {
    model: SpacewarsModel;

    onInit() {
        super.onInit();
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
    }

    onUpdate(delta: number, absolute: number) {
        if (!this.model.isPaused) {
            super.onUpdate(delta, absolute);
    
            let velocity = this.dynamics.velocity;
    
            // check boundaries and remove object out of scene
            let globalPos = this.owner.getPixiObj().toGlobal(new PIXI.Point(0,0));
            if (velocity.y > 0 && globalPos.y > this.owner.getScene().app.screen.height) {
                this.owner.remove();
                this.model.enemiesCreated--;
            }
        }
    }
}