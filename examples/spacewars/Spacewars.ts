import { PixiRunner } from '../../ts/PixiRunner'
import SpacewarsFactory from './SpacewarsFactory';
import {
    SOUND_FIRE, SOUND_GAMEOVER, SOUND_KILL, SPRITES_RESOLUTION_HEIGHT, DATA_JSON, SCENE_HEIGHT, TEXTURE_ENEMY_1, TEXTURE_ENEMY_2, TEXTURE_ENEMY_3, TEXTURE_ENEMY_4, 
    TEXTURE_ENEMY_5, TEXTURE_ENEMY_6, TEXTURE_PLAYER_1_PROJECTILE, TEXTURE_PLAYER_2_PROJECTILE, TEXTURE_ENEMY_PROJECTILE, TEXTURE_PLAYER_1, TEXTURE_PLAYER_2
} from './Constants';

class Spacewars {
    engine: PixiRunner;

    // Start a new game
    constructor() {
        this.engine = new PixiRunner();

        let canvas = (document.getElementById("gameCanvas") as HTMLCanvasElement);

        let screenHeight = canvas.height;
        
        // calculate ratio between intended resolution (here 400px of height) and real resolution
        // - this will set appropriate scale 
        let gameScale = SPRITES_RESOLUTION_HEIGHT / screenHeight;
        // scale the scene to 50 units if height
        let resolution = screenHeight / SCENE_HEIGHT * gameScale;
        this.engine.init(canvas, resolution / gameScale);

        // set global scale which has to be applied for ALL sprites as it will
        // scale them to defined unit size
        SpacewarsFactory.globalScale = 1 / resolution;

        // set resized width according to the current aspect ratio
        SpacewarsFactory.screenWidth = SCENE_HEIGHT * (canvas.width / canvas.height);
        
        // load textures
        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_PLAYER_1, 'static/spacewars/player_1.png')
            .add(TEXTURE_PLAYER_2, 'static/spacewars/player_2.png')
            .add(TEXTURE_ENEMY_1, 'static/spacewars/enemy_1.png')
            .add(TEXTURE_ENEMY_2, 'static/spacewars/enemy_2.png')
            .add(TEXTURE_ENEMY_3, 'static/spacewars/enemy_3.png')
            .add(TEXTURE_ENEMY_4, 'static/spacewars/enemy_4.png')
            .add(TEXTURE_ENEMY_5, 'static/spacewars/enemy_5.png')
            .add(TEXTURE_ENEMY_6, 'static/spacewars/enemy_6.png')
            .add(TEXTURE_PLAYER_1_PROJECTILE, 'static/spacewars/player_1_projectile.png')
            .add(TEXTURE_PLAYER_2_PROJECTILE, 'static/spacewars/player_2_projectile.png')
            .add(TEXTURE_ENEMY_PROJECTILE, 'static/spacewars/enemy_projectile.png')
            .add(SOUND_FIRE, 'static/spacewars/fire.mp3')
            .add(SOUND_GAMEOVER, 'static/spacewars/gameover.mp3')
            .add(SOUND_KILL, 'static/spacewars/kill.mp3')
            .add(DATA_JSON, 'static/spacewars/config.json')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        let factory = new SpacewarsFactory();
        factory.resetGame(this.engine.scene);
    }
}

new Spacewars();

