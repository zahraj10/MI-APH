import { ATTR_MODEL, ATTR_FACTORY } from './Constants';
import { SpacewarsModel } from './SpacewarsModel';
import Component from '../../ts/engine/Component';
import SpacewarsFactory from './SpacewarsFactory';

/**
 * Base component for all components, keeps references to model and factory
 */
export class SpacewarsBaseCmp extends Component {
    model: SpacewarsModel;
    factory: SpacewarsFactory;

    onInit() {
        this.model = this.scene.getGlobalAttribute<SpacewarsModel>(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute<SpacewarsFactory>(ATTR_FACTORY);
    }
}