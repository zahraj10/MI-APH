import {
    MSG_PROJECTILE_SHOT, MSG_ENEMY_KILLED, TAG_ENEMY, TAG_GAMEOVER, MSG_GAME_OVER, MSG_LEVEL_COMPLETED, TAG_LEVEL_COMPLETED, MSG_PAUSE, TAG_PAUSED, TAG_PROJECTILE
} from './Constants';
import Msg from '../../ts/engine/Msg';
import { SpacewarsBaseCmp } from "./SpacewarsBaseCmp";

/**
 * Manager that orchestrates the game in general
 */
export class GameManager extends SpacewarsBaseCmp {

    onInit() {
        super.onInit();
        this.subscribe(MSG_PROJECTILE_SHOT);
        this.subscribe(MSG_ENEMY_KILLED);
        this.subscribe(MSG_LEVEL_COMPLETED);
        this.subscribe(MSG_GAME_OVER);
        this.subscribe(MSG_PAUSE);
    }

    onMessage(msg: Msg) {
        if (this.model.isGameOver) {
            return;
        }

        if (msg.action == MSG_PROJECTILE_SHOT) {
            // decrease score with each shot
            this.model.score = Math.max(0, this.model.score - this.model.shootPenalty);
        } else if (msg.action == MSG_ENEMY_KILLED) {
            if (this.model.score >= this.model.goal) {
                this.sendMessage(MSG_LEVEL_COMPLETED);
            }
        } else if (msg.action == MSG_LEVEL_COMPLETED) {
            this.nextLevel();
        } else if (msg.action == MSG_PAUSE) {
            this.pause();
        } else if (msg.action == MSG_GAME_OVER) {
            this.gameOver();
        }
    }

    // shows game over info label and resets game with delay
    protected gameOver() {
        this.model.isPaused = !this.model.isPaused;

        let gameOverObj = this.scene.findFirstObjectByTag(TAG_GAMEOVER);
        gameOverObj.getPixiObj().visible = true;
        this.model.isGameOver = true;

        this.scene.invokeWithDelay(5000, () => {
            this.factory.resetGame(this.scene);
        });
    }

    // shows level completed info label and starts next level with delay
    protected nextLevel() {
        let levelCompletedObj = this.scene.findFirstObjectByTag(TAG_LEVEL_COMPLETED);
        levelCompletedObj.getPixiObj().visible = true;
        this.model.level++;
        this.model.score = 0;
        this.model.enemiesCreated = 0;
        this.model.goal += this.model.nextLevelGoalIncrease;
        this.model.isPaused = true;
        this.model.damage = Math.max(0, this.model.damage - 30);
        this.scene.invokeWithDelay(3000, () => {
            levelCompletedObj.getPixiObj().visible = false;
            this.model.enemySpawnFrequency *= 1.2;
            this.model.shotFrequency *= 1.2;
            let enemies = this.scene.findAllObjectsByTag(TAG_ENEMY);
            for (let e of enemies) {
                e.remove();
            }
            let projectiles = this.scene.findAllObjectsByTag(TAG_PROJECTILE);
            for (let p of projectiles) {
                p.remove();
            }
            this.model.isPaused = false;
        });
    }

    // pauses the game
    protected pause() {
        this.model.isPaused = !this.model.isPaused;
        let pausedObj = this.scene.findFirstObjectByTag(TAG_PAUSED);
        if (this.model.isPaused) {
            pausedObj.getPixiObj().visible = true;
        } else {
            pausedObj.getPixiObj().visible = false;
        }
    }
}