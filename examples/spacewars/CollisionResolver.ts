import {
    MSG_ENEMY_KILLED, MSG_COLLISION, TAG_ENEMY, STATE_DEAD, TAG_PLAYER_1, MSG_GAME_OVER, TAG_PLAYER_2
} from './Constants';
import Msg from '../../ts/engine/Msg';
import { CollisionInfo } from './CollisionManager';
import { SpacewarsBaseCmp } from "./SpacewarsBaseCmp";
import ChainingComponent from '../../ts/components/ChainingComponent';
import { DeathAnimation } from './DeathAnimation';
import { TAG_PROJECTILE } from '../spacewars/Constants';

/**
 * Collision resolver component
 */
export class CollisionResolver extends SpacewarsBaseCmp {

    onInit() {
        super.onInit();
        this.subscribe(MSG_COLLISION);
    }

    onMessage(msg: Msg) {
        if (this.model.isGameOver) {
            return;
        }

        if (msg.action == MSG_COLLISION) {
            this.handleCollision(msg);
        }
    }

    // handles collision with all objects
    protected handleCollision(msg: Msg) {
        let trigger = <CollisionInfo>msg.data;

        if (trigger.unit.getTag() == TAG_ENEMY) {
            // enemy-projectile collision 
            this.model.score += this.model.enemyReward;
            trigger.unit.setState(STATE_DEAD);
            this.sendMessage(MSG_ENEMY_KILLED, trigger.unit);
        } else if (trigger.unit.getTag() == TAG_PLAYER_1 || trigger.unit.getTag() == TAG_PLAYER_2) {
            if (trigger.projectile.getTag() == TAG_PROJECTILE) {
                // player-projectile collision
                this.model.damage += 5;
                trigger.unit.addComponent(new DeathAnimation());
                if (this.model.damage >= 100) {
                    this.sendMessage(MSG_GAME_OVER);
                } 
            } else {
                // player-enemy collision
                this.model.score += this.model.enemyReward;
                trigger.projectile.setState(STATE_DEAD);
                this.sendMessage(MSG_ENEMY_KILLED, trigger.projectile);
                
                this.model.damage += 15;
                trigger.unit.addComponent(new DeathAnimation());
                if (this.model.damage >= 100) {
                    this.sendMessage(MSG_GAME_OVER);
                } 
            }
        }

        // remove projectile
        trigger.projectile.addComponent(new ChainingComponent().execute((cmp) => trigger.projectile.remove())); 
    }
}