import { KeyInputComponent, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEY_SPACE, KEY_P, KEY_A, KEY_D, KEY_W, KEY_S, KEY_Q } from '../../ts/components/KeyInputComponent';
import { MSG_PROJECTILE_SHOT, MSG_PAUSE, TAG_PLAYER_1, TAG_PLAYER_2, FLAG_PLAYER_1_PROJECTILE, FLAG_PLAYER_2_PROJECTILE } from './Constants';
import { checkTime } from './Utils';
import { SpacewarsBaseCmp } from './SpacewarsBaseCmp';

const DIRECTION_LEFT = 1;
const DIRECTION_RIGHT = 2;
const DIRECTION_UP = 3;
const DIRECTION_DOWN = 4;

/**
 * Controller
 */
export class PlayerController extends SpacewarsBaseCmp {
    protected lastShot = 0;

    move(direction: number, delta: number) {
        let pixiObj = this.owner.getPixiObj();

        if (direction == DIRECTION_LEFT) {
            pixiObj.x = Math.max(3, pixiObj.x - 0.03 * delta);
        } else if (direction == DIRECTION_RIGHT) {
            pixiObj.x = Math.min(64, pixiObj.x + 0.03 * delta);
        } else if (direction == DIRECTION_UP) {
            pixiObj.y = Math.max(4, pixiObj.y - 0.03 * delta);
        } else if (direction == DIRECTION_DOWN) {
            pixiObj.y = Math.min(50, pixiObj.y + 0.03 * delta);
        }
    }

    tryFire(absolute: number): boolean {
        if (checkTime(this.lastShot, absolute, this.model.playerFireRate)) {
            this.lastShot = absolute;
            let flag = FLAG_PLAYER_1_PROJECTILE;
            if (this.owner.getTag() == TAG_PLAYER_2) {
                flag = FLAG_PLAYER_2_PROJECTILE;
            }
            this.factory.createProjectile(this.owner, this.model, flag);
            this.sendMessage(MSG_PROJECTILE_SHOT);
            return true;
        } else {
            return false;
        }
    }
}

/**
 * Keyboard input controller
 */
export class PlayerInputController extends PlayerController {
    lastKey = 0;
    
    onUpdate(delta: number, absolute: number) {
        let cmp = this.scene.stage.findComponentByClass(KeyInputComponent.name);
        let cmpKey = <KeyInputComponent><any>cmp;

        if (this.model.isPaused) {
            if (cmpKey.isKeyPressed(KEY_P) && this.owner.getTag() == TAG_PLAYER_1 && checkTime(this.lastKey, absolute, 5)) {
                this.lastKey = absolute;
                this.sendMessage(MSG_PAUSE);
            }
        } else {
            if (cmpKey.isKeyPressed(KEY_LEFT) && this.owner.getTag() == TAG_PLAYER_1) {
                this.move(DIRECTION_LEFT, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_RIGHT) && this.owner.getTag() == TAG_PLAYER_1) {
                this.move(DIRECTION_RIGHT, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_UP) && this.owner.getTag() == TAG_PLAYER_1) {
                this.move(DIRECTION_UP, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_DOWN) && this.owner.getTag() == TAG_PLAYER_1) {
                this.move(DIRECTION_DOWN, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_SPACE) && this.owner.getTag() == TAG_PLAYER_1) {
                this.tryFire(absolute);
            }
            if (cmpKey.isKeyPressed(KEY_A) && this.owner.getTag() == TAG_PLAYER_2) {
                this.move(DIRECTION_LEFT, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_D) && this.owner.getTag() == TAG_PLAYER_2) {
                this.move(DIRECTION_RIGHT, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_W) && this.owner.getTag() == TAG_PLAYER_2) {
                this.move(DIRECTION_UP, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_S) && this.owner.getTag() == TAG_PLAYER_2) {
                this.move(DIRECTION_DOWN, delta);
            }
            
            if (cmpKey.isKeyPressed(KEY_Q) && this.owner.getTag() == TAG_PLAYER_2) {
                this.tryFire(absolute);
            }

            if (cmpKey.isKeyPressed(KEY_P) && this.owner.getTag() == TAG_PLAYER_1 && checkTime(this.lastKey, absolute, 5)) {
                this.lastKey = absolute;
                this.sendMessage(MSG_PAUSE);
            }
        }
    }
}