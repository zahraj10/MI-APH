import * as PIXI from 'pixi.js';
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { MSG_OBJECT_REMOVED, MSG_OBJECT_ADDED, } from '../../ts/engine/Constants';
import Msg from '../../ts/engine/Msg';
import { MSG_COLLISION, FLAG_COLLIDABLE, STATE_DEAD, FLAG_PLAYER_1_PROJECTILE, FLAG_PLAYER_2_PROJECTILE, FLAG_ENEMY_PROJECTILE, TAG_PLAYER_1, TAG_PLAYER_2 } from './Constants';
import { SpacewarsBaseCmp } from "./SpacewarsBaseCmp";

/**
 * Entity that keeps info about a collision
 */
export class CollisionInfo {
    // hit unit
    unit: PIXICmp.ComponentObject;
    // projectile that hit given unit
    projectile: PIXICmp.ComponentObject;

    constructor(unit: PIXICmp.ComponentObject, projectile: PIXICmp.ComponentObject) {
        this.unit = unit;
        this.projectile = projectile;
    }
}

/**
 * Simple collision manager
 */
export class CollisionManager extends SpacewarsBaseCmp {
    // game object collections
    enemies = new Array<PIXICmp.ComponentObject>();
    playerProjectiles = new Array<PIXICmp.ComponentObject>();
    enemyProjectiles = new Array<PIXICmp.ComponentObject>();
    players = new Array<PIXICmp.ComponentObject>();

    onInit() {
        super.onInit();
        this.subscribe(MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_OBJECT_ADDED || msg.action == MSG_OBJECT_REMOVED) {
            // refresh collections
            this.playerProjectiles = this.scene.findAllObjectsByFlag(FLAG_PLAYER_1_PROJECTILE).concat(this.scene.findAllObjectsByFlag(FLAG_PLAYER_2_PROJECTILE));
            this.enemyProjectiles = this.scene.findAllObjectsByFlag(FLAG_ENEMY_PROJECTILE);
            this.enemies = this.scene.findAllObjectsByFlag(FLAG_COLLIDABLE);
            this.players = this.scene.findAllObjectsByTag(TAG_PLAYER_1).concat(this.scene.findAllObjectsByTag(TAG_PLAYER_2));
        }
    }

    // find collides
    onUpdate(delta, absolute) {
        let collides = new Array<CollisionInfo>();

        // projectile vs enemy
        for (let pp of this.playerProjectiles) {
            if (pp.getState() != STATE_DEAD) {
                for (let unit of this.enemies) {
                    if (unit.getState() != STATE_DEAD) {
                        let boundsA = pp.getPixiObj().getBounds();
                        let boundsB = unit.getPixiObj().getBounds();

                        let intersectionX = this.testHorizIntersection(boundsA, boundsB);
                        let intersectionY = this.testVertIntersection(boundsA, boundsB);

                        if (intersectionX > 0 && intersectionY > 0) {
                            collides.push(new CollisionInfo(unit, pp));
                        }
                    }
                }
            }
        }

        // projectile vs player
        for (let ep of this.enemyProjectiles) {
            if (ep.getState() != STATE_DEAD) {
                for (let unit of this.players) {
                    if (unit.getState() != STATE_DEAD) {
                        let boundsA = ep.getPixiObj().getBounds();
                        let boundsB = unit.getPixiObj().getBounds();
    
                        let intersectionX = this.testHorizIntersection(boundsA, boundsB);
                        let intersectionY = this.testVertIntersection(boundsA, boundsB);
    
                        if (intersectionX > 0 && intersectionY > 0) {
                            collides.push(new CollisionInfo(unit, ep));
                        }
                    }
                }
            }
        }

        // enemy vs player
        for (let ep of this.enemies) {
            if (ep.getState() != STATE_DEAD) {
                for (let unit of this.players) {
                    if (unit.getState() != STATE_DEAD) {
                        let boundsA = ep.getPixiObj().getBounds();
                        let boundsB = unit.getPixiObj().getBounds();
    
                        let intersectionX = this.testHorizIntersection(boundsA, boundsB);
                        let intersectionY = this.testVertIntersection(boundsA, boundsB);
    
                        if (intersectionX > 0 && intersectionY > 0) {
                            collides.push(new CollisionInfo(unit, ep));
                        }
                    }
                }
            }
        }

        // send message for all colliding objects
        for (let collid of collides) {
            this.sendMessage(MSG_COLLISION, collid);
        }
    }

    /**
    * Checks horizontal intersection
    */
    private testHorizIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.right, boundsB.right) - Math.max(boundsA.left, boundsB.left);
    }

    /**
     * Checks vertical intersection 
     */
    private testVertIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.bottom, boundsB.bottom) - Math.max(boundsA.top, boundsB.top);
    }
}