import { ATTR_DYNAMICS } from './../../ts/engine/Constants';
import { EnemyComponent } from './EnemyComponent';
import { ProjectileComponent } from './ProjectileComponent';
import { SoundComponent } from './SoundComponent';
import {
    ATTR_FACTORY, TAG_PLAYER_1, TEXTURE_PLAYER_1, TEXTURE_PLAYER_2, TAG_SCORE, TAG_GAMEOVER, TAG_DAMAGE, TAG_CONTROLS, TAG_PROJECTILE, TEXTURE_PLAYER_1_PROJECTILE, 
    TEXTURE_PLAYER_2_PROJECTILE, TEXTURE_ENEMY_PROJECTILE, TAG_ENEMY, ATTR_MODEL, DATA_JSON, FLAG_COLLIDABLE, MSG_ENEMY_KILLED, TEXTURE_ENEMY_1, TEXTURE_ENEMY_6, 
    TEXTURE_ENEMY_5, TEXTURE_ENEMY_4, TEXTURE_ENEMY_3, TEXTURE_ENEMY_2, TAG_LEVEL_COMPLETED, TAG_PAUSED, FLAG_ENEMY_PROJECTILE, TAG_PLAYER_2, FLAG_PLAYER_2_PROJECTILE, 
    
} from './Constants';
import { SpacewarsModel } from './SpacewarsModel';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import { CollisionManager } from './CollisionManager';
import { GameManager } from './GameManager';
import { PlayerInputController } from './PlayerController';
import PIXIObjectBuilder from '../../ts/engine/PIXIObjectBuilder';
import Vec2 from '../../ts/utils/Vec2';
import { KeyInputComponent } from '../../ts/components/KeyInputComponent';
import Scene from '../../ts/engine/Scene';
import { GenericComponent } from '../../ts/components/GenericComponent';
import Dynamics from '../../ts/utils/Dynamics';
import { EnemyMovement } from './EnemyMovement';
import ChainingComponent from '../../ts/components/ChainingComponent';
import { DeathAnimation } from './DeathAnimation';
import { CollisionResolver } from './CollisionResolver';
import { EnemySpawner } from './EnemySpawner';

export default class SpacewarsFactory {

    // global scale for sprites
    static globalScale = 1;
    // width of the screen, depends on current aspect ratio
    static screenWidth = 1;

    initializeGame(rootObject: PIXICmp.ComponentObject, model: SpacewarsModel) {

        let scene = rootObject.getScene();
        let builder = new PIXIObjectBuilder(scene);

        // ============================================================================================================
        // special component that will wait for a death of a unit, executes a DeathAnimation
        // upon it and removes it from the scene
        let deathChecker = new GenericComponent("DeathChecker") // anonymous generic component
            .doOnMessage(MSG_ENEMY_KILLED, (cmp, msg) => {    // wait for message MSG_UNIT_KILLED
                let contextObj = msg.data as PIXICmp.ComponentObject; // take the killed object from message payload
                contextObj.addComponent(new ChainingComponent() // add chaining component that will execute two closure
                    .addComponentAndWait(new DeathAnimation()) // firstly, add directly DeathAnimation to the object and wait until it finishes
                    .execute((cmp) => contextObj.remove())); // secondly, remove the object from the scene
            });
        // ============================================================================================================


        // add root components
        builder
            .withComponent(new KeyInputComponent())
            .withComponent(new GameManager())
            .withComponent(new SoundComponent())
            .withComponent(new EnemySpawner())
            .withComponent(new CollisionManager())
            .withComponent(new CollisionResolver())
            .withComponent(deathChecker)
            //.withComponent(new DebugComponent(document.getElementById("debugSect")))
            .build(rootObject);


        // create labels
        // score
        let score = new PIXICmp.Text(TAG_SCORE);
        score.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        })
        builder.relativePos(0.99, 0.99).scale(SpacewarsFactory.globalScale).anchor(1, 1)
            .withComponent(new GenericComponent("ScoreComponent").doOnUpdate((cmp, delta, absolute) => {
                let score = "Level " + model.level + ": " + model.score + "/" + model.goal;
                let text = <PIXI.Text>cmp.owner.getPixiObj();
                text.text = score;
            }))
            .build(score, rootObject);

        // game over label
        let text = "GAME OVER";
        let gameOver = new PIXICmp.Text(TAG_GAMEOVER, text);
        gameOver.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        })
        gameOver.visible = false;
        builder.relativePos(0.5, 0.5).scale(SpacewarsFactory.globalScale).anchor(0.5, 0.5).build(gameOver, rootObject);
        
        // level completed label
        text = "Level completed";
        let levelCompleted = new PIXICmp.Text(TAG_LEVEL_COMPLETED, text);
        levelCompleted.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        })
        levelCompleted.visible = false;
        builder.relativePos(0.5, 0.5).scale(SpacewarsFactory.globalScale).anchor(0.5, 0.5).build(levelCompleted, rootObject);    

        // paused label
        text = "Paused";
        let paused = new PIXICmp.Text(TAG_PAUSED, text);
        paused.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        })
        paused.visible = false;
        builder.relativePos(0.5, 0.5).scale(SpacewarsFactory.globalScale).anchor(0.5, 0.5).build(paused, rootObject);

        // damage label
        let damage = new PIXICmp.Text(TAG_DAMAGE);
        damage.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        })
        builder.relativePos(0.01, 0.99).scale(SpacewarsFactory.globalScale).anchor(0, 1)
            .withComponent(new GenericComponent("DamageComponent").doOnUpdate((cmp, delta, absolute) => {
                let damage = "Damage: " + Math.min(100, model.damage) + "%";
                let text = <PIXI.Text>cmp.owner.getPixiObj();
                text.text = damage;
            }))
            .build(damage, rootObject);
            
        // controls label
        text = "Controls:\nP - pause\nA - add 2nd player";
        let controls = new PIXICmp.Text(TAG_CONTROLS, text);
        controls.style = new PIXI.TextStyle({
            fill: "0xFFFFFF",
            fontSize: 10
        })
        controls.visible = true;
        builder.relativePos(0.15, 0.05).scale(SpacewarsFactory.globalScale).anchor(0.5, 0.5).build(controls, rootObject);

        // controls label
        text = "Player 1:\narrows - move\nSPACE - shot";
        let controls1 = new PIXICmp.Text(TAG_CONTROLS, text);
        controls1.style = new PIXI.TextStyle({
            fill: "0xFFFFFF",
            fontSize: 10
        })
        controls1.visible = true;
        builder.relativePos(0.5, 0.05).scale(SpacewarsFactory.globalScale).anchor(0.5, 0.5).build(controls1, rootObject);

        // controls label
        text = "Player 2:\nW,A,S,D - move\nQ - shot";
        let controls2 = new PIXICmp.Text(TAG_CONTROLS, text);
        controls2.style = new PIXI.TextStyle({
            fill: "0xFFFFFF",
            fontSize: 10
        })
        controls2.visible = true;
        builder.relativePos(0.8, 0.05).scale(SpacewarsFactory.globalScale).anchor(0.5, 0.5).build(controls2, rootObject);

        // player 1
        builder
            .relativePos(0.5, 0.8)
            .anchor(0.5, 1)
            .scale(0.03)
            .withComponent(new PlayerInputController())
            .build(new PIXICmp.Sprite(TAG_PLAYER_1, PIXI.Texture.fromImage(TEXTURE_PLAYER_1)), rootObject);
        
        // player 2
        builder
            .relativePos(-0.1, 0.8)
            .anchor(0.5, 1)
            .scale(0.03)
            .withComponent(new PlayerInputController())
            .build(new PIXICmp.Sprite(TAG_PLAYER_2, PIXI.Texture.fromImage(TEXTURE_PLAYER_2)), rootObject);
    }

    createProjectile(source: PIXICmp.ComponentObject, model: SpacewarsModel, flag: number) {

        let rootObject = source.getScene().stage;
        let canonPixi = source.getPixiObj();
        let height = canonPixi.getBounds().height;
        let canonGlobalPos = canonPixi.toGlobal(new PIXI.Point(0, 0));
        let velocityY = model.projectileVelocity;
        let dynamics = new Dynamics();
        
        dynamics.velocity = new Vec2(0, -velocityY);
        if (flag == FLAG_ENEMY_PROJECTILE) {
            dynamics.velocity = new Vec2(0, velocityY);
        }

        let texture = PIXI.Texture.fromImage(TEXTURE_PLAYER_1_PROJECTILE);
        if (flag == FLAG_PLAYER_2_PROJECTILE) {
            texture = PIXI.Texture.fromImage(TEXTURE_PLAYER_2_PROJECTILE);
        } else if (flag == FLAG_ENEMY_PROJECTILE) {
            texture = PIXI.Texture.fromImage(TEXTURE_ENEMY_PROJECTILE);
        }
        
        new PIXIObjectBuilder(source.getScene())
            .globalPos(canonGlobalPos.x, canonGlobalPos.y)
            .scale(SpacewarsFactory.globalScale)
            .withFlag(flag)
            .withAttribute(ATTR_DYNAMICS, dynamics)
            .withComponent(new ProjectileComponent())
            .build(new PIXICmp.Sprite(TAG_PROJECTILE, texture), rootObject);
            
    }

    createEnemy(owner: PIXICmp.ComponentObject, model: SpacewarsModel) {
        let root = owner.getScene().stage;

        let posX = Math.random() * (0.9 - 0.1) + 0.1;
        let posY = 0;
        let velocity = Math.random() * (model.enemyMaxVelocity - model.enemyMinVelocity) + model.enemyMinVelocity;
        let dynamics = new Dynamics();
        dynamics.velocity = new Vec2(0, velocity);

        let r = Math.random();
        let texture = PIXI.Texture.fromImage(TEXTURE_ENEMY_1);
        if (r > (1 / 6) * 5) {
            texture = PIXI.Texture.fromImage(TEXTURE_ENEMY_6);
        } else if (r > (1 / 6) * 4) {
            texture = PIXI.Texture.fromImage(TEXTURE_ENEMY_5);
        } else if (r > (1 / 6) * 3) {
            texture = PIXI.Texture.fromImage(TEXTURE_ENEMY_4);
        } else if (r > (1 / 6) * 2) {
            texture = PIXI.Texture.fromImage(TEXTURE_ENEMY_3);
        } else if (r > (1 / 6)) {
            texture = PIXI.Texture.fromImage(TEXTURE_ENEMY_2);
        }

        new PIXIObjectBuilder(owner.getScene())
            .withFlag(FLAG_COLLIDABLE)
            .withAttribute(ATTR_DYNAMICS, dynamics)
            .withComponent(new EnemyComponent())
            .withComponent(new EnemyMovement())
            .relativePos(posX, posY)
            .anchor(0.5, 0.5)
            .scale(SpacewarsFactory.globalScale * 0.08)
            .build(new PIXICmp.Sprite(TAG_ENEMY, texture), root);
    }

    public resetGame(scene: Scene) {
        scene.clearScene();
        let model = new SpacewarsModel();
        model.loadModel(PIXI.loader.resources[DATA_JSON].data);
        model.reset();
        scene.addGlobalAttribute(ATTR_FACTORY, this);
        scene.addGlobalAttribute(ATTR_MODEL, model);
        this.initializeGame(scene.stage, model);
    }
}