import { MSG_ANIM_ENDED } from './Constants';
import { checkTime } from './Utils';
import { SpacewarsBaseCmp } from './SpacewarsBaseCmp';

/**
 * Simple flicker animation that only changes visibility of the object
 */
export class DeathAnimation extends SpacewarsBaseCmp {
    lastSwitch = 0;
    totalSw = 0;

    onUpdate(delta, absolute) {
        if (checkTime(this.lastSwitch, absolute, 50)) {
            this.lastSwitch = absolute;
            this.owner.getPixiObj().visible = !this.owner.getPixiObj().visible;

            if (this.totalSw++ > 4) {
                this.finish();
                this.sendMessage(MSG_ANIM_ENDED);
            }
        }
    }
}